package com.clone.flappy.bird.ui

import com.badlogic.gdx.InputProcessor
import com.clone.flappy.bird.objects.Fly


//для управления мухой
//InputProcessor является интерфейсом между нашим кодом и кроссплатформенным кодом
//когда наше устройство получает касание оно вызывает метод InputProcessor
class InputHandler(
        private val fly: Fly?
) : InputProcessor {


    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    override fun keyTyped(character: Char): Boolean {
        return false
    }

    override fun scrolled(amount: Int): Boolean {
        return false
    }

    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false
    }

    override fun keyDown(keycode: Int): Boolean {
        return false
    }

    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        fly?.onClick()
        return false
    }
}