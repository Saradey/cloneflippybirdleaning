package com.clone.flappy.bird.tools

import aurelienribon.tweenengine.TweenAccessor

class ValueAccessor : TweenAccessor<Value> {

    //получаем значение которое мы хотим изменить для нашего объекта
    //и помещаем их в массив с которым TweenEngine выполняет преобразования
    //измененое значение передаем в объект

    override fun getValues(target: Value?, tweenType: Int, returnValues: FloatArray?): Int {
        returnValues?.set(0, target?.value ?: 0f)
        return 1
    }


    override fun setValues(target: Value?, tweenType: Int, newValues: FloatArray?) {
        target?.value = newValues?.get(0) ?: 0f
    }


}