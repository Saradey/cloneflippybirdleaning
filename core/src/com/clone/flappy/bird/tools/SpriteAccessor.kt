package com.clone.flappy.bird.tools

import aurelienribon.tweenengine.TweenAccessor
import com.badlogic.gdx.graphics.g2d.Sprite


//для каждого класса для которого мы собираемя менять с помощь tween engine
//нужно создать отдельный tweenAccessor
//этот меняет прозрачность
class SpriteAccessor : TweenAccessor<Sprite> {

    companion object {
        const val ALPHA = 1
    }

    
    override fun getValues(target: Sprite?, tweenType: Int, returnValues: FloatArray?): Int {
        when (tweenType) {
            ALPHA -> {
                target?.let {
                    //берем альфа канал данного спрайта и ложим в лист над которыми будут производиться
                    //операции интерполяции
                    returnValues?.set(0, it.color.a)
                    return 1
                }
            }
        }
        return 0
    }


    override fun setValues(target: Sprite?, tweenType: Int, newValues: FloatArray?) {
        when (tweenType) {
            ALPHA -> {
                target?.setColor(1f, 1f, 1f, newValues?.get(0) ?: 0f)
            }
        }
    }


}