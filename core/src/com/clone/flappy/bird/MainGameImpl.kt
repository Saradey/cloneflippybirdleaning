package com.clone.flappy.bird

import com.badlogic.gdx.Game
import com.badlogic.gdx.Screen
import com.clone.flappy.bird.di.component.AppComponent
import com.clone.flappy.bird.di.component.DaggerAppComponent
import com.clone.flappy.bird.di.module.ResourceModule
import com.clone.flappy.bird.loader.ResourceLoader
import com.clone.flappy.bird.screens.GameWorldScreen
import com.clone.flappy.bird.screens.SplashScreen
import javax.inject.Inject


//Класс Game в libGDX является абстрактным и предоставляет для использования реализацию
// ApplicationListener, наряду со вспомогательными методами для обработки
// визуализации экрана.
//по сути это аля класс Application в Android
class MainGameImpl : Game(), IMainGame {

    @Inject
    lateinit var resource: ResourceLoader


    companion object {
        lateinit var component: AppComponent
    }

    //вызывается один раз при создании приложения
    override fun create() {
        component = DaggerAppComponent.builder()
                .resourceModule(ResourceModule())
                .build()

        component.inject(this)

        setScreen(SplashScreen(this))
    }


    override fun dispose() {
        resource.dispose()
    }


    override fun goToTheGameWorldScreen() {
        setScreen(GameWorldScreen())
    }

}