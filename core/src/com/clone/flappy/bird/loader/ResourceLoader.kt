package com.clone.flappy.bird.loader

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas


/**
 * @author Evgeny Goncharov
 * класс загрузки ресурсов
 */
class ResourceLoader {

    lateinit var atlas: TextureAtlas
    lateinit var logo: Sprite
    lateinit var background: Sprite
    lateinit var grass: Sprite
    lateinit var fly1: Sprite
    lateinit var fly2: Sprite
    lateinit var fly3: Sprite
    lateinit var spider: Sprite
    lateinit var webUp: Sprite
    lateinit var webDown: Sprite
    lateinit var retry: Sprite
    lateinit var gameOver: Sprite
    lateinit var buttonOff: Sprite
    lateinit var buttonOn: Sprite
    lateinit var flyAndSpyders: Sprite
    lateinit var highScore: Sprite
    lateinit var starOff: Sprite
    lateinit var starOn: Sprite
    lateinit var tapToFly: Sprite
    lateinit var wood: Sprite

    /*
     * для анимации
     */
    lateinit var animation: Animation<Sprite>

    /*
     * звуки
     */
    private lateinit var dead: Sound
    private lateinit var flap: Sound
    private lateinit var coin: Sound
    private lateinit var fail: Sound

    /*
     * фоновая музыка
     */
    private lateinit var flyMusic: Music


    /*
     * шрифты
     */
    private lateinit var font: BitmapFont
    private lateinit var shadow: BitmapFont
    private lateinit var whiteFont: BitmapFont

    init {
        load()
    }

    /*
     * загрузка всех ресурсов
     */
    //доступны как статические поля
    //Application запускает приложение и сообщает api client о событиях уровня приложения
    //таких как изменение размера окна
    //Files предоставляет лежащий в основе платформы файловую систему
    //Input информирует клиент  пользовательском вводе
    //Audio предоставляет средства к воспроизведению музыки
    //Graphics предоставляет openGL для установки видео режимов

    //к примеру: GDX.audio.newAudioDevice(44100, false)
    //GDX.audio является бекэнд реализацией которая была создана экземпляром application
    //при запуске приложения
    private fun load() {
        //создаем спрайты
        atlas = TextureAtlas(Gdx.files.internal("image/pack.atlas"), true)
        //Класс Sprite регион текстуры
        background = Sprite(atlas.findRegion("background"))
        buttonOff = Sprite(atlas.findRegion("buttonOff"))
        buttonOn = Sprite(atlas.findRegion("buttonOn"))
        fly1 = Sprite(atlas.findRegion("fly1"))
        fly2 = Sprite(atlas.findRegion("fly2"))
        fly3 = Sprite(atlas.findRegion("fly3"))
        flyAndSpyders = Sprite(atlas.findRegion("flyAndSpyders"))
        gameOver = Sprite(atlas.findRegion("gameOver"))
        grass = Sprite(atlas.findRegion("grass"))
        highScore = Sprite(atlas.findRegion("highScore"))
        logo = Sprite(atlas.findRegion("logo"))
        //по умолчанию openGL отрисовывает все перевернута, для этого нам нужно указать эти значения
        logo.flip(false, true)
        retry = Sprite(atlas.findRegion("retry"))
        spider = Sprite(atlas.findRegion("spider"))
        starOff = Sprite(atlas.findRegion("starOff"))
        starOn = Sprite(atlas.findRegion("starOn"))
        tapToFly = Sprite(atlas.findRegion("tapToFly"))
        webDown = Sprite(atlas.findRegion("webDown"))
        webUp = Sprite(atlas.findRegion("webUp"))
        wood = Sprite(atlas.findRegion("wood"))

        //создаем анимацию
        animation = Animation(0.06f, fly1, fly2, fly3)
        animation.playMode = Animation.PlayMode.LOOP_PINGPONG

        //создаем звуки
        //звуки он полностью загружает в память
        dead = Gdx.audio.newSound(Gdx.files.internal("sounds/dead.wav"))
        flap = Gdx.audio.newSound(Gdx.files.internal("sounds/flap.wav"))
        coin = Gdx.audio.newSound(Gdx.files.internal("sounds/coin.wav"))
        fail = Gdx.audio.newSound(Gdx.files.internal("sounds/fall.wav"))

        //создаем музыку
        //музыку он загружает лишь частично, проигрывает потоком
        flyMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/fly.mp3"))

        //создаем шрифты
        font = BitmapFont(Gdx.files.internal("fonts/text.fnt"))
        font.data.setScale(.25f, -.25f)
        whiteFont = BitmapFont(Gdx.files.internal("fonts/whitetext.fnt"))
        whiteFont.data.setScale(.1f, -.1f)
        shadow = BitmapFont(Gdx.files.internal("fonts/shadow.fnt"))
        shadow.data.setScale(.25f, -.25f)
    }


    //освобождение памяти от ненужны ресурсов
    fun dispose() {
        atlas.dispose()

        dead.dispose()
        flap.dispose()
        coin.dispose()
        fail.dispose()
        flyMusic.dispose()

        font.dispose()
        whiteFont.dispose()
        shadow.dispose()
    }

}