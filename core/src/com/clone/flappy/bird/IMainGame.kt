package com.clone.flappy.bird

import com.badlogic.gdx.Screen

interface IMainGame {

    fun goToTheGameWorldScreen()

}