package com.clone.flappy.bird.screens

import aurelienribon.tweenengine.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.clone.flappy.bird.IMainGame
import com.clone.flappy.bird.MainGameImpl
import com.clone.flappy.bird.loader.ResourceLoader
import com.clone.flappy.bird.tools.SpriteAccessor
import javax.inject.Inject


//Экраны имеют основополагающее значение для любой игры с несколькими
// компонентами. Экраны содержат много методов, которые вы использовали в
// ApplicationListener объектах, и включают в себя пару новых методов
// show и hide, которые вызываются при получении и потери фокуса соответственно.

class SplashScreen(
        var game: IMainGame
) : Screen {

    lateinit var manager: TweenManager
    //отрисовывает текстуры и спрайты можно оптимизировать
    lateinit var batch: SpriteBatch
    lateinit var sprite: Sprite

    @Inject
    lateinit var resource: ResourceLoader

    init {
        MainGameImpl.component.inject(this)
    }

    //вызывается перед создание скрина
    override fun hide() {
    }


    //вызывается после создания скрина
    override fun show() {
        sprite = Sprite(resource.logo)
        sprite.setColor(1f, 1f, 1f, 0f)

        //размеры экрана
        //width это ширина
        //height это высота
        val width = Gdx.graphics.width
        val height = Gdx.graphics.height
        //маштабируем картинку по длине
        //1000 * 0.7 = 700
        val desiredWidth = width * 0.7f
        //берем масштабируемый размер и делим на размер картинки
        //700 / 350 = 2
        val scale = desiredWidth / sprite.width


        //маштабируем нашу картинку
        //350 * 2 = 700                         350 * 2 = 700
        sprite.setSize(sprite.width * scale, sprite.height * scale)
        //устанавливаем размер и позицию лога по центру
        //1000 / 2 - 350 / 2 = 500 - 175 = 325
        //1800 / 2 - 350 / 2 = 900 - 175 = 725
        sprite.setPosition(
                (width / 2f) - (sprite.width / 2f),
                (height / 2f) - (sprite.height / 2f)
        )

        setupTween()

        //нужен для отрисовки
        batch = SpriteBatch()
    }


    //для преобразования
    private fun setupTween() {
        //регистрируем новый спрайт аксесор котоорый будет изменять наш твин
        Tween.registerAccessor(Sprite::class.java, SpriteAccessor())
        //нам также нужен менедлжер он будет произхводить интерполяцию используя SpriteAccessor

        manager = TweenManager()

        //его методы будут вызываться после окончания преобразования
        val tweenCallback = TweenCallback { type, source ->
            //здесь переход на другой контроллер
            game.goToTheGameWorldScreen()
        }

        //код преобразования
        //берем наш спрайт и увеличиваем прозрачность от 0 до 1
        //а затем снова уменьшаем до нуля
        //duration время выполнения всей операции
        //SpriteAccessor.ALPHA индификатор
        //sprite изменяемый объект
        Tween.to(sprite, SpriteAccessor.ALPHA, 1f)
                .target(1f)
                .ease(TweenEquations.easeInCubic)
                //repeatYoyo задержка и через сколько оно должно будет повториться
                .repeatYoyo(1, 0.4f)
                //наш колбек который сработает после завершения
                .setCallback(tweenCallback)
                //когда должен сработать наш колбек
                .setCallbackTriggers(TweenCallback.COMPLETE)
                //говорим какому менеджеру стартовать
                .start(manager)
    }


    //здесь происходит отрисовка
    override fun render(delta: Float) {
        manager.update(delta)
        //обновляем экран белым цветом
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        batch.begin()
        sprite.draw(batch)
        batch.end()
    }


    //на android вызывается когда нажата кнопка home или при входящем звонке
    //на десктопе вызывается перед выходом из приложения перед методом dispose
    override fun pause() {
    }

    //вызывается только на android когда приложение возобновляет из состояние приостановки
    override fun resume() {
    }

    //вызывается при каждом изменение размера экрана игры когда игра не находиться в состояние паузы
    //вызывается один раз сразу после метода create
    override fun resize(width: Int, height: Int) {
    }

    //вызывается когда срин уничтожается
    //ему предшествует вызов метода pause
    override fun dispose() {
    }
}