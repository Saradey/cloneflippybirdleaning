package com.clone.flappy.bird.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.math.Vector2
import com.clone.flappy.bird.MainGameImpl
import com.clone.flappy.bird.game.IGameRender
import com.clone.flappy.bird.game.IGameWorld
import com.clone.flappy.bird.objects.Fly
import com.clone.flappy.bird.ui.InputHandler
import javax.inject.Inject

class GameWorldScreen : Screen {

    @Inject
    lateinit var render: IGameRender

    @Inject
    lateinit var world: IGameWorld

    var runTime: Float = 0f


    init {
        MainGameImpl.component.inject(this)
    }


    override fun hide() {
    }


    override fun show() {
        //наша игра будет работать на многочисленных android устройствах которые имею разный размер
        //экрана, поэтому необходимо корректно обрабатывать разрпешение эранов устройства
        //что бы этого добиться мы установим фиксированную ширину в 136 пикселей
        //а высота будет задаваться динамически после определения разрешения экрана устройства

        val screenWidth = Gdx.graphics.width
        val screenHeight = Gdx.graphics.height
        //фиксированную ширину в 136 пикселей
        val gameWidth = 135f
        //а высота будет задаваться динамически после определения разрешения экрана устройства
        val gameHeight = screenHeight / (screenWidth / gameWidth)

        //центр экрана
        //эти значения мы будем передавать игровому миру для
        //размещения некоторых объектов на экране
        val midPointY = (gameHeight / 2).toInt()
        val midPointX = (gameWidth / 2).toInt()

        world.setMidPointY(midPointY)
        world.setMidPointX(midPointX)
        world.initFly()

        render.setMidPointY(midPointY)
        render.setMidPointX(midPointX)
        render.setGameHeight(gameHeight.toInt())
        render.initCamera()


        //создаем хендлер и прикручиваем его к игре
        Gdx.input.inputProcessor = InputHandler(world.getFly())
    }


    override fun render(delta: Float) {
        runTime += delta

        world.update(delta)
        render.render(delta, runTime)
    }


    override fun pause() {
    }


    override fun resume() {
    }


    override fun resize(width: Int, height: Int) {
    }


    override fun dispose() {
    }

}