package com.clone.flappy.bird.di.component

import com.clone.flappy.bird.MainGameImpl
import com.clone.flappy.bird.di.module.GameComponentModule
import com.clone.flappy.bird.di.module.ResourceModule
import com.clone.flappy.bird.game.GameRenderImpl
import com.clone.flappy.bird.screens.GameWorldScreen
import com.clone.flappy.bird.screens.SplashScreen
import dagger.Component
import javax.inject.Singleton

@Component(modules = [ResourceModule::class, GameComponentModule::class])
@Singleton
interface AppComponent {

    fun inject(main: MainGameImpl)

    fun inject(screen: SplashScreen)
    fun inject(screen: GameWorldScreen)

    fun inject(gameRender : GameRenderImpl)

}