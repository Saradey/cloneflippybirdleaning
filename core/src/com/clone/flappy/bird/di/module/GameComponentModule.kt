package com.clone.flappy.bird.di.module

import com.clone.flappy.bird.game.GameRenderImpl
import com.clone.flappy.bird.game.GameWorldImpl
import com.clone.flappy.bird.game.IGameRender
import com.clone.flappy.bird.game.IGameWorld
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class GameComponentModule {

    @Singleton
    @Provides
    fun provideGameWorld(): IGameWorld = GameWorldImpl()

    @Singleton
    @Provides
    fun provideGameRender(world: IGameWorld): IGameRender = GameRenderImpl(world as GameWorldImpl)

}