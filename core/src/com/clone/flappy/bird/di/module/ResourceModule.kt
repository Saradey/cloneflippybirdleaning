package com.clone.flappy.bird.di.module

import com.clone.flappy.bird.loader.ResourceLoader
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ResourceModule {

    @Provides
    @Singleton
    fun provideResource(): ResourceLoader = ResourceLoader()

}