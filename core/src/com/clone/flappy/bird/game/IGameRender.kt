package com.clone.flappy.bird.game

interface IGameRender {

    fun render(delta : Float, runTime : Float)

    fun setMidPointY(midPointY: Int)

    fun setMidPointX(midPointX: Int)

    fun setGameHeight(gameHeight: Int)

    fun initCamera()

}