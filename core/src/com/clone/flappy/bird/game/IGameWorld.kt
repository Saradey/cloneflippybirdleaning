package com.clone.flappy.bird.game

import com.clone.flappy.bird.objects.Fly

interface IGameWorld {

    fun update(delta: Float)

    fun setMidPointY(midPointY: Int)

    fun setMidPointX(midPointX: Int)

    fun initFly()

    fun getFly(): Fly?

}