package com.clone.flappy.bird.game

import com.badlogic.gdx.math.Vector2
import com.clone.flappy.bird.objects.Fly

//класс служит для обновления мира
class GameWorldImpl : IGameWorld {

    private var midPointY = 0
    private var midPointX = 0

    private var fly: Fly? = null


    override fun update(delta: Float) {
        fly?.update(delta)
    }

    override fun setMidPointY(midPointY: Int) {
        this.midPointY = midPointY
    }

    override fun setMidPointX(midPointX: Int) {
        this.midPointX = midPointX
    }


    override fun initFly() {
        fly = Fly(
                position = Vector2(33f, (midPointY - 5).toFloat()),
                width = 17f,
                height = 12f)
    }


    override fun getFly(): Fly? {
        return fly
    }

}