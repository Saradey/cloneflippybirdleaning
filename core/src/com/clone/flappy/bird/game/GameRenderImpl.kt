package com.clone.flappy.bird.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.clone.flappy.bird.MainGameImpl
import com.clone.flappy.bird.loader.ResourceLoader
import javax.inject.Inject

//класс для отрисовки мира
class GameRenderImpl(
        val world: GameWorldImpl
) : IGameRender {

    init {
        MainGameImpl.component.inject(this)
    }


    @Inject
    lateinit var loader: ResourceLoader


    private var midPointY = 0
    private var midPointX = 0
    private var gameHeight = 0

    //отчет координат, начинается с левого верхнего угла
    private lateinit var camera: OrthographicCamera

    //рисует прямоугольник залитый одним цветом
    private lateinit var shapeRender: ShapeRenderer
    //отрисовать текстуру
    private lateinit var spriteBatch: SpriteBatch

    //ортаганальная камера используется в 2д играх и реализует ортоганальную проекцию
    //то есть параллельну/ проекцию для отображения объектов игрового мира на двумерной плоскости
    //в независимости от того где объекты находяться в игровом мире, далеко или близко от наблюдателя

    //runTime хрониться значение продолжительности игры
    override fun render(delta: Float, runTime: Float) {
        val fly = world.getFly()
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        shapeRender.begin(ShapeRenderer.ShapeType.Filled)

        //отрисовка фонового цвета
        shapeRender.setColor(186f / 255.0f, 224f / 255f, 213f / 255f, 1f)
        shapeRender.rect(0f, 0f, 136f, midPointY + 66f)

        //отрисуем прямоугольник травы
        shapeRender.setColor(167f / 255.0f, 211f / 255f, 152f / 255f, 1f)
        shapeRender.rect(0f, midPointY + 66f, 136f, 11f)

        //отрисуем прямоугольник воды
        shapeRender.setColor(75f / 255.0f, 136f / 255f, 178f / 255f, 1f)
        shapeRender.rect(0f, midPointY + 77f, 136f, 53f)
        shapeRender.end()

        spriteBatch.begin()
        //отменим прозрачность
        //так как это нужно для повышение производительности
        spriteBatch.disableBlending()
        //рисуем фоновую каротинку
        spriteBatch.draw(loader.background, 0f, midPointY + 23f, 136f, 43f)
        //нашей мухи нужна прозрачность поэтому включаем ее
        spriteBatch.enableBlending()

        //отрисуем муху в ее координатах
        //и проанимируем муху
        fly?.let {
            spriteBatch.draw(loader.animation.getKeyFrame(runTime),
                    it.position.x,
                    it.position.y,
                    it.width,
                    it.height
            )
        }
        spriteBatch.end()
    }


    override fun setMidPointY(midPointY: Int) {
        this.midPointY = midPointY
    }


    override fun setMidPointX(midPointX: Int) {
        this.midPointX = midPointX
    }


    override fun setGameHeight(gameHeight: Int) {
        this.gameHeight = gameHeight
    }


    override fun initCamera() {
        camera = OrthographicCamera()
        //устанавливаем ортогональную проекцию и задаем размеры игрового мира
        camera.setToOrtho(true, 136f, gameHeight.toFloat())

        spriteBatch = SpriteBatch()
        spriteBatch.projectionMatrix = camera.combined
        shapeRender = ShapeRenderer()
        shapeRender.projectionMatrix = camera.combined
    }

}